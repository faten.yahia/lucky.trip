import Description from "./Description";
import Thumbail from "./Thumbail";


export default class Destination {
    constructor(
        public id: string,
        public image_url: string,
        public city: string,
        public country_name: string,
        public description: Description,
        public thumbail: Thumbail     
        ){}
}