import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TripService } from 'src/app/services/trip.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  id: string = this.activatedRoute.snapshot.params["id"];
  result:any;
  value:string="";

  constructor(private activatedRoute: ActivatedRoute,private tripService:TripService,private router:Router) { }

  ngOnInit(): void {
    localStorage.clear();
    this.tripService.result(this.id).subscribe(
      (res:any) => {
        console.log("img ",res.destination.thumbnail.image_url)
        this.result=res;
      }
    )
  }

  search(){
    localStorage.setItem('search',this.value);
    this.router.navigateByUrl('');
  }



  

}
