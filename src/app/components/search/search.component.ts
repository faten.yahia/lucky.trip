import { Component, OnInit } from '@angular/core';
import Destination from '../models/Destination';
import { TripService } from 'src/app/services/trip.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  value: string = "";
  destinations: Destination[];

  constructor(
    private tripService: TripService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(localStorage.getItem('search') == null)
    {this.default();} else {
      this.value=localStorage.getItem('search')
      this.tripService.search(localStorage.getItem('search')).subscribe(
        (res: any) => {
          this.destinations = (res.destinations);
        }
      )
    }
  }

  default() {
    this.tripService.search("").subscribe(
      (res: any) => {
        this.destinations = (res.destinations);
      }
    )
  }

  search() {
    this.tripService.search(this.value).subscribe(
      (res: any) => {
        this.destinations = (res.destinations);
      }
    )
  }

  detail(id:any) {
    this.router.navigateByUrl("result/"+id);
  }

}
