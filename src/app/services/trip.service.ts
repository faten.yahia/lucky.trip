import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(private http:HttpClient) { }

  search(value:String){
    return this.http.get(`https://devapi.luckytrip.co.uk/api/2.0/top_five/destinations?search_type=city_or_country&search_value=${value}`)
  }

  result(id:String){
    return this.http.get(`https://devapi.luckytrip.co.uk/api/2.0/top_five/destination?id=${id}`)
  }
  
}
